import React, { Component } from 'react';
import { Collapse } from 'antd';

const { Panel } = Collapse;

  
class DicoListPage extends Component {

    render() {
        return (
            <div className="dicoList">
            <Collapse accordion>
                <Panel header="This is panel header 1" key="1">
                <p style={{ paddingLeft: 24 }}>
                            A dog is a type of domesticated animal. Known for its loyalty and faithfulness, it can be found
                            as a welcome guest in many households across the world.
                        </p>
                </Panel>
                <Panel header="This is panel header 2" key="2">
                <p style={{ paddingLeft: 24 }}>
                            A dog is a type of domesticated animal. Known for its loyalty and faithfulness, it can be found
                            as a welcome guest in many households across the world.
                        </p>
                </Panel>
                <Panel header="This is panel header 3" key="3">
                <p style={{ paddingLeft: 24 }}>
                            A dog is a type of domesticated animal. Known for its loyalty and faithfulness, it can be found
                            as a welcome guest in many households across the world.
                        </p>
                </Panel>
            </Collapse>
            </div>
            
        );
    }
}

export default DicoListPage;
