import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import SplitText from 'react-pose-text';
import './HomePage.css';

const charPoses = {
    exit: { y: 30, opacity: 0 },
    enter: {
      y: 0,
      opacity: 1,
      transition: ({ charInWordIndex }) => ({
        type: 'spring',
        delay: charInWordIndex * 30,
        stiffness: 500 + charInWordIndex * 150,
        damping: 10 - charInWordIndex * 1
      })
    }
  };

  
class HomePage extends Component {
    render() {
        return (
            <div className="header-content">
                {/* <h1 className="header-title">Rime X</h1>
                <div class="wrapper">
                    <div class="focus">
                    SAMI
                    </div>
                    <div class="mask">
                        <div class="text">SAMI</div>
                    </div>
                    </div>
                <h3 className="header-subtitle">L'album de l'année est dans les bacs!</h3>
                <Link to={`/otherPageExample`} className="Menu-list-item">
                    <SplitText initialPose="exit" pose="enter" charPoses={charPoses} className="link-split-text">
                        ENTRER
                    </SplitText>
                    <span class="Mask"><span>ENTRER</span></span>
                    <span class="Mask"><span>ENTRER</span></span>

                </Link> */}
                <h1 className="header-title">SAM DICTIONARY</h1>
                <div class="wrapper">
                    <div class="focus">
                    BIENVENUE
                    </div>
                    <div class="mask">
                        <div class="text">BIENVENUE</div>
                    </div>
                    </div>
                <h3 className="header-subtitle">Le meilleur endroit pour apprendre à être un chicks magnet!</h3>
                <Link to={`/dico`} className="Menu-list-item">
                    <SplitText initialPose="exit" pose="enter" charPoses={charPoses} className="link-split-text">
                        ENTRER
                    </SplitText>
                    <span class="Mask"><span>ENTRER</span></span>
                    <span class="Mask"><span>ENTRER</span></span>

                </Link>
            </div>
  
        );
    }
}

export default HomePage;
