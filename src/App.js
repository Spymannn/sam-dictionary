import React, { Component } from 'react';
import './App.css';
import { Route, Switch , BrowserRouter} from "react-router-dom";

import Sound from 'react-sound';
import music from './assets/sounds/01.mp3';
import { MdPauseCircleFilled, MdPlayCircleFilled } from 'react-icons/md';


import HomePage from './containters/HomePage/HomePage'
import OtherPageExample from './containters/OtherPageExample/OtherPageExample'
import NotFoundPage from './containters/NotFoundPage/NotFoundPage'
import DicoListPage from './containters/DicoListPage/DicoListPage'


class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
        musicStatus: 'PLAYING',
    }
  }

  stopPlayMusic = () => {
    console.warn('here');
    if (this.state.musicStatus === 'PLAYING') {
        this.setState({musicStatus: 'PAUSED'})
    } else {
        this.setState({musicStatus: 'PLAYING'})
    }
  }

  render() {
    return (
      <div className="App">
         <Sound
            url={music}
            playStatus={this.state.musicStatus}
            onFinishedPlaying={this.stopPlayMusic}
          />
          
          <BrowserRouter>
            <content>
                {/* <Header /> */}
                {/* <Menu logoutHandler={this.logoutHandler.bind(this)}/> */}
                <Switch>
                  <Route path='/' exact component={HomePage} />
                  <Route path='/home' exact component={HomePage} />
                  <Route path='/otherPageExample' exact component={OtherPageExample} />
                  <Route path='/dico' exact component={DicoListPage} />
                  <Route component={NotFoundPage} />
                </Switch> 
                {/* <Footer /> */}
            </content> 
          </BrowserRouter>     

          {this.state.musicStatus === 'PLAYING' ? 
              <MdPauseCircleFilled className="music" onClick={this.stopPlayMusic}/> 
              : 
              <MdPlayCircleFilled className="music" onClick={this.stopPlayMusic}/> 
          }
         
      </div>
    );
  }
}

export default App;
